/************************************************************************
File: RS232.c
Description:
Driver for the serial RS232 interface of the ATMEGA644x microprocessor.
************************************************************************/

#include "RS232.h"

struct RS232Struct {
  TRS232Number rs232Number;
  TRingBuffer sendBuffer;
  TRingBuffer receiveBuffer;
};

// Static variables
static TRS232 globalRS232[ERS232_NO_LAST] = {NULL, NULL};

/******************** Private Function Declarations ********************/

/**
 *
 */
static void RS232InitConnection0(TRS232 aRS232,
                                 unsigned long aCpuClk,
                                 TRS232Config* aConfig);

/**
 *
 */
static void RS232InitConnection1(TRS232 aRS232,
                                 unsigned long aCpuClk,
                                 TRS232Config* aConfig);

/**
 * Calculate the value for the baudrate registers (ubrrn).
 */
static unsigned int RS232CalcUBRR(unsigned long cpuClk,
                                  unsigned int baudrate,
                                  TBool* needsDoubleSpeed);

/**
 * @brief This function calculates the relative error caused by
 *        ubrr only being an integer and not a floating point value.
 *
 * @return The relative error in %.
 */
static double RS232CalcBaudrateError(unsigned long cpuClk,
                                     unsigned int baudrate,
                                     unsigned int ubrr,
                                     TBool needsDoubleSpeed);

/******************** Public Function Definitions ********************/
void RS232ConfigDefaultInit(TRS232Config* aConfig) {}

TRS232 RS232Create(TRS232Number aRS232Number,
                   unsigned long aCpuClk,
                   TRS232Config aConfig) {
  if (aRS232Number >= ERS232_NO_LAST)
    return NULL;

  TRS232 rs232 = (TRS232)calloc(1, sizeof(struct RS232Struct));

  if (NULL == rs232)
    return NULL;

  rs232->rs232Number = aRS232Number;
  rs232->sendBuffer = RingBufferCreate(aConfig.sendBufferSize);
  rs232->receiveBuffer = RingBufferCreate(aConfig.receiveBufferSize);

  if (NULL == rs232->sendBuffer || NULL == rs232->receiveBuffer) {
    RS232Destroy(rs232);
    return NULL;
  }

  // Disable/Clear all Interrupts
  cli();

  switch (aRS232Number) {
    default:
    case ERS232_NO_0:
      RS232InitConnection0(rs232, aCpuClk, &aConfig);
      break;

    case ERS232_NO_1:
      RS232InitConnection1(rs232, aCpuClk, &aConfig);
      break;
  }

  if (NULL != globalRS232[aRS232Number])
    RS232Destroy(globalRS232[aRS232Number]);

  globalRS232[aRS232Number] = rs232;

  // Enable all Interrupts
  sei();

  return rs232;
}

void RS232Destroy(TRS232 aRS232) {
  if (NULL == aRS232)
    return;

  cli();

  if (NULL != aRS232->sendBuffer)
    RingBufferDestroy(aRS232->sendBuffer);

  if (NULL != aRS232->receiveBuffer)
    RingBufferDestroy(aRS232->receiveBuffer);

  // Turn off interrupt
  switch (aRS232->rs232Number) {
    default:
    case ERS232_NO_0:
      UCSR0B = 0;
      break;

    case ERS232_NO_1:
      UCSR1B = 0;
      break;
  }

  globalRS232[aRS232->rs232Number] = NULL;

  free(aRS232);

  sei();
}

unsigned char RS232Send(TRS232 aRS232,
                        unsigned char* aBuffer,
                        unsigned char aBufferSize) {
  unsigned char byteNo;

  for (byteNo = 0; byteNo < aBufferSize; byteNo++) {
    if (!RingBufferWrite(aRS232->sendBuffer, aBuffer[byteNo]))
      break;
  }

  if (byteNo > 0) {
    switch (aRS232->rs232Number) {
      default:
      case ERS232_NO_0:
        UCSR0B |= (1 << UDRIE0);
        break;

      case ERS232_NO_1:
        UCSR1B |= (1 << UDRIE1);
        break;
    }
  }

  return byteNo;
}

unsigned char RS232Receive(TRS232 aRS232,
                           unsigned char* aBuffer,
                           unsigned char aBufferSize) {
  unsigned char data;
  unsigned char i;

  for (i = 0; i < aBufferSize; i++) {
    if (RingBufferRead(aRS232->receiveBuffer, &data)) {
      aBuffer[i] = data;
    } else {
      break;
    }
  }

  return i;
}

TRS232Error RS232GetLastError(TRS232 aRS232) {
  return ERS232_ERROR_NONE;
}

/******************** Private Function Definitions ********************/
static void RS232InitConnection0(TRS232 aRS232,
                                 unsigned long aCpuClk,
                                 TRS232Config* aConfig) {
  TBool needsDoubleSpeed;

  UCSR0A = 0;
  UCSR0B = 0;
  UCSR0C = 0;

  // UBRR0 is a 12 bit register
  UBRR0 = RS232CalcUBRR(aCpuClk, aConfig->baudrate, &needsDoubleSpeed);
  if (needsDoubleSpeed)
    UCSR0A |= (1 << U2X0);

  // Set the number of data bits to 8 bit
  UCSR0C |= (3 << UCSZ00);

  // RXCIE0: Writing this bit to one enables interrupt on the UCSRnA.RXC Flag.
  // RXEN0: Writing this bit to one enables the USART Receiver.
  // TXEN0: Writing this bit to one enables the USART Transmitter.
  UCSR0B |= (1 << RXCIE0) | (1 << RXEN0) | (1 << TXEN0);
}

static void RS232InitConnection1(TRS232 aRS232,
                                 unsigned long aCpuClk,
                                 TRS232Config* aConfig) {
  TBool needsDoubleSpeed;

  UCSR1A = 0;
  UCSR1B = 0;
  UCSR1C = 0;

  // UBRR1 is a 12 bit register
  UBRR1 = RS232CalcUBRR(aCpuClk, aConfig->baudrate, &needsDoubleSpeed);
  if (needsDoubleSpeed)
    UCSR1A |= (1 << U2X1);

  // Set the number of data bits to 8 bit
  UCSR1C |= (3 << UCSZ10);

  // RXCIE1: Writing this bit to one enables interrupt on the UCSRnA.RXC Flag.
  // RXEN1: Writing this bit to one enables the USART Receiver.
  // TXEN1: Writing this bit to one enables the USART Transmitter.
  UCSR1B |= (1 << RXCIE1) | (1 << RXEN1) | (1 << TXEN1);
}

static unsigned int RS232CalcUBRR(unsigned long cpuClk,
                                  unsigned int baudrate,
                                  TBool* needsDoubleSpeed) {
  double ubrr = (((double)cpuClk) / ((double)(16UL * baudrate))) - 1;
  *needsDoubleSpeed = EFALSE;

  // If the absolute value of the relative error is greater than,
  // or equal to 0.5%, use double speed.
  if (0.5 <= fabs(RS232CalcBaudrateError(cpuClk, baudrate, (unsigned int)ubrr,
                                         *needsDoubleSpeed))) {
    // We have to recalculate ubrr because just multiplying it by 2,
    // is not precise enough.
    ubrr = (((double)cpuClk) / ((double)(8UL * baudrate))) - 1;
    *needsDoubleSpeed = ETRUE;
  }

  ubrr = (double)lrint(ubrr);

  return ((unsigned int)ubrr);
}

static double RS232CalcBaudrateError(unsigned long cpuClk,
                                     unsigned int baudrate,
                                     unsigned int ubrr,
                                     TBool needsDoubleSpeed) {
  double actualBaudrate = (((double)cpuClk) / ((double)(16UL * (ubrr + 1))));
  if (needsDoubleSpeed)
    actualBaudrate *= 2;

  double error = (actualBaudrate / ((double)baudrate)) - 1;
  error *= 100;

  error = (double)lrint(error * 10) / 10;

  return error;
}

/******************** Handling Interrupts ********************/
ISR(USART0_UDRE_vect) {
  unsigned char data;

  if (RingBufferRead(globalRS232[ERS232_NO_0]->sendBuffer, &data)) {
    // Data from UDR0 gets shifted out by a ShiftRegister

    UDR0 = data;
  } else {
    // Disable interrupt, because there is no more data
    UCSR0B &= ~(1 << UDRIE0);
  }
}

ISR(USART1_UDRE_vect) {
  unsigned char data;

  if (RingBufferRead(globalRS232[ERS232_NO_1]->sendBuffer, &data)) {
    UDR1 = data;
  } else {
    // Disable interrupt, because there is no more data
    UCSR1B &= ~(1 << UDRIE1);
  }
}

ISR(USART0_RX_vect) {
  RingBufferWrite(globalRS232[ERS232_NO_0]->receiveBuffer, UDR0);
}

ISR(USART1_RX_vect) {
  RingBufferWrite(globalRS232[ERS232_NO_1]->receiveBuffer, UDR1);
}
