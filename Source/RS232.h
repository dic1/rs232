/************************************************************************
File: RS232.h
Description:
        Driver for the serial RS232 interface of the ATMEGA644x microprocessor.
************************************************************************/

#ifndef RS232_H
#define RS232_H

#include "HtlStddef.h"
#include "RingBuffer.h"
#include <avr/interrupt.h>
#include <avr/io.h>
#include <math.h>

typedef struct RS232Struct* TRS232;

typedef struct {
  unsigned char sendBufferSize;
  unsigned char receiveBufferSize;
  unsigned int baudrate;
} TRS232Config;

typedef enum {
  ERS232_NO_0,
  ERS232_NO_1,
  ERS232_NO_LAST,
} TRS232Number;

typedef enum {
  ERS232_ERROR_NONE,
  ERS232_ERROR_OVERFLOW,
} TRS232Error;

void RS232ConfigDefaultInit(TRS232Config* aConfig);

/**
 * Function: RS232Create
 *
 * Description:
 *	The created RS232 Object must be destroyed, with the function
 *	RS232Destroy() if not used anymore.
 *
 * Parameters:
 *	aRS232Number - The internal number of the UART interface
 *	of the microprocessor.
 *	aCpuClk - Your processors clock speed.
 *	aConfig - The configuration structure for the RS232 interface.
 *
 * Returns:
 *	The RS232 Object if successful, otherwise NULL.
 */
TRS232 RS232Create(TRS232Number aRS232Number,
                   unsigned long aCpuClk,
                   TRS232Config aConfig);
/**
 * Function: RS232Destroy
 *
 * Description:
 *
 * Parameters:
 *	aRS232 - The RS232 structure you want to destroy.
 */
void RS232Destroy(TRS232 aRS232);

/**
 * Function: RS232Send
 *
 * Description:
 *
 * Parameters:
 *
 * Returns:
 *	How many bytes were successfully sent.
 */
unsigned char RS232Send(TRS232 aRS232,
                        unsigned char* aBuffer,
                        unsigned char aBufferSize);

/**
 * Function: RS232Receive
 *
 * Description:
 *
 * Parameters:
 *
 * Returns:
 *	How many bytes were successfully received.
 */
unsigned char RS232Receive(TRS232 aRS232,
                           unsigned char* aBuffer,
                           unsigned char aBufferSize);
/**
 * Function: RS232GetLastError
 *
 * Description:
 *
 * Parameters:
 *
 * Returns:
 *
 */
TRS232Error RS232GetLastError(TRS232 aRS232);

#endif
