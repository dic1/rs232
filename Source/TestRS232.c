/*
 * RS232.c
 *
 * Created: 15/01/2021 14:46:30
 * Author : Emanuel Oberholzer
 */

#include "HtlStddef.h"
#include "RS232.h"
#include <avr/io.h>
#include <string.h>

void testRS232_1(void);
void testRS232_2(void);
void testRS232Echo(void);

int main(void) {
  // testRS232_1();
  // testRS232_2();
  testRS232Echo();
}

void testRS232_1(void) {
  TRS232 rs232;
  TRS232Config config;

  config.baudrate = 9600;
  config.receiveBufferSize = 20;
  config.sendBufferSize = 20;

  rs232 = RS232Create(ERS232_NO_0, F_CPU, config);

  while (1) {
    RS232Send(rs232, (unsigned char*)"12345 ", 6);
    DelayMs(1000);
  }

  RS232Destroy(rs232);
}

void testRS232_2(void) {
  TRS232 rs232;
  TRS232Config config;
  const unsigned char bufferSize = 20;

  config.baudrate = 9600;
  config.receiveBufferSize = 20;
  config.sendBufferSize = 20;

  rs232 = RS232Create(ERS232_NO_0, F_CPU, config);

  unsigned char message[bufferSize];

  unsigned char catChar[2];
  catChar[1] = '\0';

  while (1) {
    strcpy((char*)message, "Sending: ");
    strcat((char*)message, (char*)catChar);
    strcat((char*)message, "\n");

    RS232Send(rs232, message, strlen((char*)message));
    DelayMs(1000);

    catChar[0]++;
    if (0 == catChar[0])
      catChar[0] = 1;
  }

  RS232Destroy(rs232);
}

void testRS232Echo(void) {
  TRS232 rs232;
  TRS232Config config;

  config.baudrate = 9600;
  config.receiveBufferSize = 40;
  config.sendBufferSize = 40;

  const unsigned char bufferSize = 40;
  unsigned char buffer[bufferSize];

  unsigned char received = 0;

  rs232 = RS232Create(ERS232_NO_0, F_CPU, config);

  while (1) {
    received = RS232Receive(rs232, buffer, bufferSize);

    DelayMs(250);

    // received data.
    // Send it back
    if (0 < received) {
      unsigned char message[bufferSize];
      strcpy((char*)message, "Received: ");

      const unsigned char maxBufferLength =
          bufferSize - 2 - strlen((char*)message);

      strncat((char*)message, (char*)buffer,
              (maxBufferLength <= received) ? maxBufferLength : received);
      strcat((char*)message, "\n");

      RS232Send(rs232, message, strlen((char*)message));
    }
  }

  RS232Destroy(rs232);
}

